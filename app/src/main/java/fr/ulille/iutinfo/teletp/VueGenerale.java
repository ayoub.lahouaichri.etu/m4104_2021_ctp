package fr.ulille.iutinfo.teletp;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {
    // TODO Q1

    private View itemView;
    public String poste  ;
    public String DISTANCEL ;
    public String salle ;
    // TODO Q2.c
    private SuiviViewModel model ;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View itemView, Bundle savedInstanceState) {
        super.onViewCreated(itemView, savedInstanceState);
        // TODO Q1
        this.itemView = itemView;
        this.DISTANCEL = itemView.getContext().getResources().getStringArray(R.array.list_salles)[0] ;
        this.poste = "" ;
        this.salle = DISTANCEL ;
        // TODO Q2.c
        this.model = new ViewModelProvider(this).get(SuiviViewModel.class) ;
        // TODO Q4
        Spinner salle = ((Spinner) itemView.findViewById(R.id.spSalle)) ;
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(itemView.getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        salle.setAdapter(adapter);

        Spinner poste = ((Spinner) itemView.findViewById(R.id.spPoste)) ;
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(itemView.getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        poste.setAdapter(adapter2);



        itemView.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            String name = ((TextView) itemView.findViewById(R.id.tvLogin) ).getText().toString() ;
            model.setUsername(name);

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();
        /*
        salle.setOnItemSelectedListener(view -> {
            update() ;
        }) ;
        */

        // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner salle = (Spinner) itemView.findViewById(R.id.spSalle) ;
        Spinner poste = ((Spinner) itemView.findViewById(R.id.spPoste)) ;


            if ( salle.getSelectedItem().toString().equals(DISTANCEL)) {
                poste.setVisibility(View.GONE);
                poste.setEnabled(false);
                model.setLocalisation("Distanciel");
            }else {
                poste.setVisibility(View.VISIBLE);
                poste.setEnabled(true);
                model.setLocalisation(salle.getSelectedItem().toString()+ " : "+poste.getSelectedItem().toString());
            }


    }
    // TODO Q9
}